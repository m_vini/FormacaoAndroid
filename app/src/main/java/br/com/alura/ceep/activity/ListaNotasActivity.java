package br.com.alura.ceep.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.alura.ceep.R;
import br.com.alura.ceep.activity.recyclerview.adapter.ListNotasAdapterRecycler;
import br.com.alura.ceep.activity.recyclerview.adapter.helper.callback.NotaItemTouchHelperCallback;
import br.com.alura.ceep.dao.NotaDAO;
import br.com.alura.ceep.model.Cor;
import br.com.alura.ceep.model.Nota;
import br.com.alura.ceep.preferences.NotasPreference;

import static br.com.alura.ceep.activity.NotaActivityConstantes.CHAVE_NOTA;
import static br.com.alura.ceep.activity.NotaActivityConstantes.REQUEST_CREATE_NOTA;
import static br.com.alura.ceep.activity.NotaActivityConstantes.REQUEST_EDIT_NOTA;


public class ListaNotasActivity extends AppCompatActivity {
    public static final String TITLE_APP_BAR = "Notas";
    RecyclerView recyclerView;
    private List<Nota> notas = null;
    private ListNotasAdapterRecycler adapter;
    NotasPreference notasPreference;

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        notas = (List<Nota>) savedInstanceState.getSerializable("notas");
        configuraAdapter(this.notas);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("notas", Serializable.class.cast(notas));
    }

    private ArrayList<Cor> obterArrayCores() {
        ArrayList<Cor> cores = null;
        if (notas != null) {
            cores = new ArrayList<Cor>();
            for (Nota nota : notas) {
                cores.add(nota.getCor());
            }
        }
        return cores;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_notas);
        notasPreference = new NotasPreference(ListaNotasActivity.this);

        notas = getNotas();
        notasPreference.removeListNotas("notas");

        configuraRecyclerView(notas);

        setTitle(TITLE_APP_BAR);

        TextView textView = findViewById(R.id.lista_notas_insere_nota);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListaNotasActivity.this, FormularioNotaActivity.class);
                startActivityForResult(intent, REQUEST_CREATE_NOTA);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lista_notas, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        carregaLayoutSalvo(menu);
        return true;
    }

    private void carregaLayoutSalvo(Menu menu) {
        String layout = notasPreference.getPreferenceLayout();
        MenuItem item = menu.getItem(0);
        if (layout.equals("Linear")) {
            carregaLinearLayout(item);
        } else {
            carregaStaggeredLayout(item);
        }
    }

    private void alteraLayout(MenuItem item, String layout) {
        if (layout.equals("Linear")) {
            carregaStaggeredLayout(item);
        } else {
            carregaLinearLayout(item);
        }
    }

    private void carregaLinearLayout(MenuItem item) {
        item.setTitle("Linear");
        item.setIcon(R.drawable.ic_staggered_layout);
        notasPreference.salvaLayout("Linear");
        recyclerView.setLayoutManager(new LinearLayoutManager(ListaNotasActivity.this));
    }

    private void carregaStaggeredLayout(MenuItem item) {
        item.setIcon(R.drawable.ic_linear_layout);
        item.setTitle("Staggered");
        notasPreference.salvaLayout("Staggered");
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_lista_opcao_layout:
                alteraLayout(item, item.getTitle().toString());
                break;
            case R.id.menu_feedback:
                Intent intent = new Intent(ListaNotasActivity.this, FeedbackActivity.class);
                startActivity(intent);
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CREATE_NOTA) {
            if (resultCode == Activity.RESULT_OK && data.hasExtra(CHAVE_NOTA)) {
                Nota nota = (Nota) data.getSerializableExtra(CHAVE_NOTA);
                adapter.insere(nota);
            }
        }
        if (requestCode == REQUEST_EDIT_NOTA) {
            if (resultCode == RESULT_OK
                    && data.hasExtra(CHAVE_NOTA)) {
                Nota nota = (Nota) data.getSerializableExtra(CHAVE_NOTA);
                adapter.altera(nota);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private List<Nota> getNotas() {
        NotaDAO dao = new NotaDAO(ListaNotasActivity.this);

        return dao.todos();
    }

    private void configuraRecyclerView(List<Nota> notas) {
        recyclerView = findViewById(R.id.lista_notas_recycler_view);
        configuraAdapter(this.notas);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new NotaItemTouchHelperCallback(adapter, ListaNotasActivity.this));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    private void configuraAdapter(List<Nota> notas) {
        adapter = new ListNotasAdapterRecycler(this, notas);
        adapter.setClickListener(new ListNotasAdapterRecycler.OnItemClickListener() {
            @Override
            public void onItemClick(Nota nota, int position) {
                Intent abreFormularioComNota = new Intent(ListaNotasActivity.this, FormularioNotaActivity.class);
                abreFormularioComNota.putExtra(CHAVE_NOTA, nota);
                startActivityForResult(abreFormularioComNota, REQUEST_EDIT_NOTA);
            }
        });
        recyclerView.setAdapter(adapter);
    }
}
