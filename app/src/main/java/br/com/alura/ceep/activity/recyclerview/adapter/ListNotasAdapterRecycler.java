package br.com.alura.ceep.activity.recyclerview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.alura.ceep.R;
import br.com.alura.ceep.dao.NotaDAO;
import br.com.alura.ceep.model.Cor;
import br.com.alura.ceep.model.Nota;

public class ListNotasAdapterRecycler extends RecyclerView.Adapter<ListNotasAdapterRecycler.NotaViewHolder> {

    private Context context;
    private List<Nota> notas;
    private OnItemClickListener clickListener;

    public ListNotasAdapterRecycler(Context context, List<Nota> notas) {
        this.context = context;
        this.notas = notas;
    }

    @NonNull
    @Override
    public ListNotasAdapterRecycler.NotaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_nota, parent, false);

        return new NotaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListNotasAdapterRecycler.NotaViewHolder holder, int position) {
        Nota nota = notas.get(position);
        holder.preecherDados(nota);
    }

    @Override
    public int getItemCount() {
        return notas.size();
    }

    public void altera(Nota nota) {
        notas.set(nota.getPosicao(), nota);
        notifyItemChanged(nota.getPosicao());
    }

    public void remove(Nota nota) {
        int itemRemovido = new NotaDAO(context).remove(nota);
        if (itemRemovido > 0) {
            notas.remove(nota);
            decrementarPosicoes(nota.getPosicao());
            notifyItemRemoved(nota.getPosicao());
        }
    }

    private void decrementarPosicoes(int posicao) {
        List<Nota> notasParaAtualizar = new ArrayList<>();
        for (Nota nota :
                notas) {
            if (nota.getPosicao() > posicao) {
                nota.setPosicao(nota.getPosicao() - 1);
                notasParaAtualizar.add(nota);
            }
        }
        if (notasParaAtualizar.size() > 0) {
            NotaDAO dao = new NotaDAO(context);
            dao.decrementarPosicoes(notasParaAtualizar);
        }
    }

    public void troca(Nota notaInicial, Nota notaFinal) {
        int posicaoInicial = notaInicial.getPosicao();
        int posicaoFinal = notaFinal.getPosicao();

        new NotaDAO(context).troca(notaInicial, notaFinal);

        notas.get(posicaoInicial).setPosicao(posicaoFinal);
        notas.get(posicaoFinal).setPosicao(posicaoInicial);

        Collections.swap(notas, posicaoInicial, posicaoFinal);


        notifyItemMoved(posicaoInicial, posicaoFinal);


    }

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void insere(Nota nota) {
        final int default_position = 0;
        if (notas.size() > 0)
            incrementarPosicoes(notas);

        notas.add(default_position, nota);
        notifyItemInserted(default_position);
    }

    private void incrementarPosicoes(List<Nota> notas) {
        NotaDAO dao = new NotaDAO(context);
        for (Nota nota : notas) {
            nota.setPosicao(nota.getPosicao() + 1);
        }
        dao.incrementarPosicoes(notas);
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void preencheCorNotas(ArrayList<Cor> listaCoresSaved) {
        for (int i = 0; i < notas.size(); i++) {
            notas.get(i).setCor(listaCoresSaved.get(i));
        }
        notifyDataSetChanged();
    }

    class NotaViewHolder extends RecyclerView.ViewHolder {
        private TextView titulo;
        private TextView descricao;
        private Nota nota;
        private View view;

        public NotaViewHolder(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.item_nota_titulo);
            descricao = itemView.findViewById(R.id.item_nota_descricao);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(nota, getAdapterPosition());
                }
            });
            view = itemView;
        }

        private void preecherDados(Nota nota) {
            this.nota = nota;
            //insere os valores nos campos da tela
            titulo.setText(nota.getTitulo());
            descricao.setText(nota.getDescricao());
            if (nota.getCor() != null) {
                int codigoCor = Color.parseColor(nota.getCor().getCodigo());
                view.setBackgroundColor(codigoCor);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Nota nota, int position);
    }

}
