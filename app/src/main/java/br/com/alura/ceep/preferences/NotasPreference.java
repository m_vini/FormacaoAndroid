package br.com.alura.ceep.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class NotasPreference {
    public static final String NOTAS_PREFERENCE = "br.com.alura.ceep.preferences.NotasPreference";
    public static final String CHAVE_LAYOUT = "layout";
    public static final String CHAVE_ACESSO = "acesso";
    public static final String LAYOUT_DEFAULT = "Linear";
    Context context;

    public NotasPreference(Context context) {
        this.context = context;
    }

    public void salvaLayout(String s) {
        SharedPreferences preferences = getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(CHAVE_LAYOUT, s);
        editor.commit();
    }

    public void registraAcesso(){
        SharedPreferences preferences = getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean(CHAVE_ACESSO, true);
        editor.commit();
    }

    public boolean primeiroAcesso(){
        SharedPreferences preferences = getSharedPreferences();
        return !preferences.contains(CHAVE_ACESSO);

    }


    public String getPreferenceLayout(){
        SharedPreferences preferences = getSharedPreferences();
        return preferences.getString(CHAVE_LAYOUT, LAYOUT_DEFAULT);
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(NOTAS_PREFERENCE, context.MODE_PRIVATE);
    }

    public void removeListNotas(String key) {
        SharedPreferences preferences = getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();

        editor.remove(key);
        editor.commit();
    }
}
