package br.com.alura.ceep.activity.recyclerview.adapter.helper.callback;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import br.com.alura.ceep.activity.recyclerview.adapter.ListNotasAdapterRecycler;
import br.com.alura.ceep.model.Nota;

public class NotaItemTouchHelperCallback extends ItemTouchHelper.Callback {
    private ListNotasAdapterRecycler adapter;
    private Context context;

    public NotaItemTouchHelperCallback(ListNotasAdapterRecycler adapter, Context context) {
        this.adapter = adapter;
        this.context = context;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int movementFlags = ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT;
        int movementDrags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        return makeMovementFlags(movementDrags, movementFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        Nota nota1 = adapter.getNotas().get(viewHolder.getAdapterPosition());
        Nota nota2 = adapter.getNotas().get(target.getAdapterPosition());
        adapter.troca(nota1, nota2);
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        Nota nota = adapter.getNotas().get(viewHolder.getAdapterPosition());
        adapter.remove(nota);
    }
}
