package br.com.alura.ceep.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import br.com.alura.ceep.R;
import br.com.alura.ceep.preferences.NotasPreference;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Handler handler = new android.os.Handler();
        int tempoSplash = obterTempoSplash();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                telaInicial();
            }
        }, tempoSplash);
    }

    private void telaInicial() {
        Intent intent = new Intent(this, ListaNotasActivity.class);
        startActivity(intent);
        finish();
    }

    private int obterTempoSplash() {
        NotasPreference notasPreference = new NotasPreference(SplashScreenActivity.this);
        if (notasPreference.primeiroAcesso()) {
            notasPreference.registraAcesso();
            return 2000;
        }
        return 500;
    }

}
