package br.com.alura.ceep.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.List;

import br.com.alura.ceep.R;
import br.com.alura.ceep.activity.recyclerview.adapter.ListCoresAdapter;
import br.com.alura.ceep.dao.NotaDAO;
import br.com.alura.ceep.model.Cor;
import br.com.alura.ceep.model.Nota;

import static br.com.alura.ceep.activity.NotaActivityConstantes.CHAVE_NOTA;

public class FormularioNotaActivity extends AppCompatActivity {
    public static final String TITLE_APP_BAR_INSERE_NOTAS = "Insere Notas";
    public static final String TITLE_APP_BAR_ALTERA_NOTAS = "Altera Notas";
    private Nota nota;
    private TextView titulo;
    private TextView descricao;
    private Integer posicaoItem;
    private RecyclerView recyclerViewCores;
    private ListCoresAdapter adapter;
    private ConstraintLayout constraintLayoutPrincipal;
    private Cor corPadrao = new Cor().getCorDefault();
    private Cor corAtual;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("corAtual", corAtual);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_nota);
        bindFields();
        setTitle(TITLE_APP_BAR_INSERE_NOTAS);

        if (savedInstanceState != null) {
            Cor corExtra = (Cor) savedInstanceState.getSerializable("corAtual");
            adicionaCorNota(corExtra);
        }
        Intent intent = getIntent();
        if (intent.hasExtra(CHAVE_NOTA)) {
            setTitle(TITLE_APP_BAR_ALTERA_NOTAS);
            nota = (Nota) intent.getSerializableExtra(CHAVE_NOTA);
            preencheCampos();
        } else {
            nota = new Nota();
        }

        if (!existeCorNotaSelecionada()) {
            adicionaCorNota(corPadrao);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_formulario_nota_salva, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_formulario_nota_salva:
                if (posicaoItem == null)
                    nota = criarNota();
                else
                    nota = alterarNota();

                nota.setCor(corAtual);
                retornaNota();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    private Nota criarNota() {
        Nota nota = new Nota(titulo.getText().toString(), descricao.getText().toString());
        Long id = new NotaDAO(FormularioNotaActivity.this).insere(nota);
        if (id != null) {
            nota.setId(id);
            return nota;
        } else {
            return null;
        }
    }

    private Nota alterarNota() {
        nota.setTitulo(titulo.getText().toString());
        nota.setDescricao(descricao.getText().toString());
        int linhasAlteradas = new NotaDAO(FormularioNotaActivity.this).altera(nota);
        if (linhasAlteradas > 0)
            return nota;
        else
            return null;
    }

    private void retornaNota() {
        Intent resultado = new Intent();
        resultado.putExtra(CHAVE_NOTA, nota);
        setResult(RESULT_OK, resultado);
    }

    private void bindFields() {
        titulo = findViewById(R.id.formulario_titulo);
        descricao = findViewById(R.id.formulario_descricao);
        recyclerViewCores = findViewById(R.id.formulario_recycler_cores);
        constraintLayoutPrincipal = findViewById(R.id.formulario_nota_layout);
        configuraAdapter();
    }

    private void configuraAdapter() {
        List<Cor> cores = new Cor().getCores();
        adapter = new ListCoresAdapter(FormularioNotaActivity.this, cores);
        adapter.setOnItemClickListener(new ListCoresAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Cor cor, int position) {
                adicionaCorNota(cor);
            }
        });
        recyclerViewCores.setAdapter(adapter);
    }

    private void adicionaCorNota(Cor cor) {
        corAtual = cor;
        preencheCor(cor);
    }

    private void preencheCor(Cor cor) {
        int codigoCor = Color.parseColor(cor.getCodigo());
        constraintLayoutPrincipal.setBackgroundColor(codigoCor);
    }

    public boolean existeCorNotaSelecionada() {
        return constraintLayoutPrincipal.getBackground() != null;
    }

    private void preencheCampos() {
        posicaoItem = nota.getPosicao();
        titulo.setText(nota.getTitulo());
        descricao.setText(nota.getDescricao());
        if (corAtual != null) {
            preencheCor(corAtual);
        } else {
            if (nota.getCor() != null) {
                adicionaCorNota(nota.getCor());
            }
        }

    }
}
