package br.com.alura.ceep.activity.recyclerview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.alura.ceep.R;
import br.com.alura.ceep.model.Cor;

public class ListCoresAdapter extends RecyclerView.Adapter<ListCoresAdapter.CorViewHolder> {
    private Context context;
    private List<Cor> cores;
    private OnItemClickListener onItemClickListener;

    public ListCoresAdapter(Context context, List<Cor> cores) {
        this.context = context;
        this.cores = cores;
    }

    @NonNull
    @Override
    public CorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_item_cor, parent, false);

        return new CorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CorViewHolder holder, int position) {
        Cor cor = cores.get(position);
        holder.preencherDados(cor);
    }

    @Override
    public int getItemCount() {
        return cores.size();
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    class CorViewHolder extends RecyclerView.ViewHolder {
        View view;
        Cor cor;

        public CorViewHolder(View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.item_cor);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(cor, getAdapterPosition());
                }
            });

        }

        private void preencherDados(Cor cor) {
            this.cor = cor;
            int codigo = Color.parseColor(cor.getCodigo());
            Drawable color = view.getBackground();
            color.setTint(codigo);
            view.setBackground(color);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Cor cor, int position);
    }
}
