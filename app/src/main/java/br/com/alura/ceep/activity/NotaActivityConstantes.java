package br.com.alura.ceep.activity;

public interface NotaActivityConstantes {
    String CHAVE_NOTA= "nota";
    String CHAVE_POSITION= "position";
    final int REQUEST_CREATE_NOTA = 1;
    final int REQUEST_EDIT_NOTA = 2;
}
