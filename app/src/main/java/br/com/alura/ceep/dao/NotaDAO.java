package br.com.alura.ceep.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.alura.ceep.model.Nota;

public class NotaDAO extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "notasdb";
    private static final String TABLE_NAME = "notas";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_TITULO = "titulo";
    private static final String COLUMN_DESCRICAO = "descricao";
    private static final String COLUMN_POSICAO = "posicao";

    public NotaDAO(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_TITULO + " text," +
                COLUMN_DESCRICAO + " text," +
                COLUMN_POSICAO + " int DEFAULT 0)";

        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public List<Nota> todos() {
        String sql = "SELECT * FROM Notas ORDER BY " + COLUMN_POSICAO;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        List<Nota> notas = new ArrayList<Nota>();
        while (cursor.moveToNext()) {
            Nota nota = preencheNotaCursor(cursor);
            notas.add(nota);
        }
        return notas;
    }

    @NonNull
    private Nota preencheNotaCursor(Cursor cursor) {
        return new Nota(
                cursor.getLong(cursor.getColumnIndex(COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TITULO)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRICAO)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_POSICAO)));
    }

    public long insere(Nota nota) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_TITULO, nota.getTitulo());
        values.put(COLUMN_DESCRICAO, nota.getDescricao());

        long id = db.insert(TABLE_NAME, null, values);

        return id;
    }

    public int altera(Nota nota) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_TITULO, nota.getTitulo());
        values.put(COLUMN_DESCRICAO, nota.getDescricao());

        return db.update(TABLE_NAME, values, COLUMN_ID + " = ? ", new String[]{String.valueOf(nota.getId())});
    }

    public int remove(Nota nota) {
        SQLiteDatabase db = getWritableDatabase();

        return db.delete(TABLE_NAME, COLUMN_ID + " = ? ", new String[]{String.valueOf(nota.getId())});
    }

    public void troca(Nota nota1, Nota nota2) {
        if (nota1 != null && nota2 != null) {
            alteraPosicoes(nota1, nota2);
        }
    }

    private void alteraPosicoes(Nota nota1, Nota nota2) {
        SQLiteDatabase db = getWritableDatabase();
        //dados para nota 1
        ContentValues values1 = new ContentValues();
        values1.put(COLUMN_POSICAO, nota2.getPosicao());
        //dados para nota 2
        ContentValues values2 = new ContentValues();
        values2.put(COLUMN_POSICAO, nota1.getPosicao());

        db.update(TABLE_NAME, values1, COLUMN_ID + " = ?", new String[]{String.valueOf(nota1.getId())});
        db.update(TABLE_NAME, values2, COLUMN_ID + " = ?", new String[]{String.valueOf(nota2.getId())});

    }

    private Nota getNotaDatabase(int posicao) {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM notas where posicao = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(posicao)});
        Nota nota = null;
        if (cursor.moveToNext()) {
            nota = preencheNotaCursor(cursor);
        }
        return nota;
    }

    public void incrementarPosicoes(List<Nota> notas) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();
        for (Nota nota :
                notas) {
            values.put(COLUMN_POSICAO, nota.getPosicao());
            db.update(TABLE_NAME, values, COLUMN_ID + " = ?", new String[]{String.valueOf(nota.getId())});
        }
    }

    public void decrementarPosicoes(List<Nota> notas) {
        ContentValues values = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();
        for (Nota nota :
                notas) {
            values.put(COLUMN_POSICAO, String.valueOf(nota.getPosicao()));
            int update = db.update(TABLE_NAME, values, COLUMN_ID + " = ?", new String[]{String.valueOf(nota.getId())});
        }
    }
}
