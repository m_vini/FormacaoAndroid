package br.com.alura.ceep.model;

import java.io.Serializable;

public class Nota implements Serializable {
    private Long id;
    private String titulo;
    private String descricao;
    private int posicao;
    private Cor cor;

    public Nota() {
    }

    public Nota(Long id, String titulo, String descricao, int posicao) {
        this.id = id;
        this.titulo = titulo;
        this.descricao = descricao;
        this.posicao = posicao;
    }

    public Nota(String titulo, String descricao) {
        this.titulo = titulo;
        this.descricao = descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getPosicao() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao = posicao;
    }

    public Cor getCor() {
        return cor;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }
}
