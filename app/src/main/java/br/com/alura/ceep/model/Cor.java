package br.com.alura.ceep.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cor implements Serializable{
    private final String AZUL = "#408EC9";
    private final String BRANCO = "#ffffff";
    private final String VERMELHO = "#EC2F4B";
    private final String VERDE = "#9ACD32";
    private final String AMARELO = "#f9f256";
    private final String LILAS = "#f1cbff";
    private final String CINZA = "#d2d4dc";
    private final String MARROM = "#a47c48";
    private final String ROXO = "#be29ec";

    private String nome;
    private String codigo;

    public Cor() {
    }

    public Cor(String nome, String codigo) {
        this.nome = nome;
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Cor> getCores(){
        List<Cor> cores = new ArrayList<>();
        cores.add(new Cor("BRANCO",BRANCO));
        cores.add(new Cor("AZUL",AZUL));
        cores.add(new Cor("AMARELO",AMARELO));
        cores.add(new Cor("VERMELHO",VERMELHO));
        cores.add(new Cor("VERDE",VERDE));
        cores.add(new Cor("LILAS",LILAS));
        cores.add(new Cor("CINZA",CINZA));
        cores.add(new Cor("MARROM",MARROM));
        cores.add(new Cor("ROXO",ROXO));

        return cores;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Cor getCorDefault(){
        return new Cor("BRANCO",BRANCO);
    }

}
